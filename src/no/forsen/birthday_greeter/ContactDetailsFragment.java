package no.forsen.birthday_greeter;

import java.text.DateFormat;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class ContactDetailsFragment extends Fragment {
	EditText firstname, lastname, phonenumber, birthdate;
	private Uri uri; 
	public long bdate;
	private ActionMode acMode;
	
	private ActionMode.Callback modeCallBack = new ActionMode.Callback(){
		private boolean save = true; 
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu){
			return false; 
		}
		@Override
		public void onDestroyActionMode(ActionMode mode){
			doneClicked(save);
		}
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu){
			mode.getMenuInflater().inflate(R.menu.context_existing_user, menu);
			return true; 
		}
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item){
			switch(item.getItemId()){
			case R.id.action_delete:
				try{
					getActivity().getContentResolver().delete(uri, null, null);
				} catch (Exception e){
					// nothing to delete
				}
				save =false; 
			}
			mode.finish();
			return false;
		}
	};

	public static ContactDetailsFragment newInstance(int id){
		ContactDetailsFragment f = new ContactDetailsFragment();
		Bundle args = new Bundle();
		args.putInt("id", id); 
		f.setArguments(args);
		
		return f; 
	}
	
	public int getShownId() {
		return getArguments().getInt("id",0);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		if (container == null) {
			return null; 
		}
		
		String[] projection = 
		{
			DBAdapter.FIRSTNAME,
			DBAdapter.LASTNAME,
			DBAdapter.PHONENUMBER,
			DBAdapter.BIRTHDATE
		};
		
		uri = Uri.parse("content://" + DBAdapter.PROVIDER +"/contact/"+(getShownId()));
		Cursor cur = getActivity().getContentResolver().query(
				uri,projection,null,null,null);
		
		DateFormat df = android.text.format.DateFormat.getDateFormat(getActivity().getApplicationContext());
		View view = inflater.inflate(R.layout.fragment_contactdetails, container, false);
		
		firstname = (EditText) view.findViewById(R.id.detail_edit_firstname);
		lastname = (EditText) view.findViewById(R.id.detail_edit_lastname);
		phonenumber = (EditText) view.findViewById(R.id.detail_edit_phonenumber);
		birthdate = (EditText) view.findViewById(R.id.detail_edit_birthdate);
		
		final Bundle args = new Bundle();
		if( cur != null && cur.moveToFirst()){
			acMode = getActivity().startActionMode(modeCallBack);
			firstname.setText(cur.getString(0));
			lastname.setText(cur.getString(1));
			phonenumber.setText(cur.getString(2));
			bdate = cur.getLong(3); 
			birthdate.setText(df.format(bdate));
			args.putLong("currentDate", bdate);
			cur.close();
		}
		else{
			if(acMode != null)
				acMode.finish();
			return inflater.inflate(R.layout.empty,container,false); 	
		}
			
		birthdate.setClickable(true); 
		/*
		birthdate.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				DialogFragment newFragment = new DatePickerFragment();
				newFragment.setArguments(args); 
				newFragment.show(getFragmentManager(), "datepicker" ); 
			}
		});
		*/
		birthdate.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus)
				{
					DialogFragment newFragment = new DatePickerFragment();
					newFragment.setArguments(args);
					newFragment.show(getFragmentManager(),"datepicker"); 
				}
				
			}
			
		});
		firstname.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s){
				Validation.hasText(firstname,getString(R.string.required)); 
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
			
		});

		phonenumber.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s){
				Validation.isPhoneNumber(phonenumber,true,getString(R.string.phone_msg),getString(R.string.required)); 
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
			}
			
		});
		return view; 
	}
	
	private void doneClicked(boolean save){
		if(!save)
			dismiss();
		
		ContentValues cv = new ContentValues(); 
		cv.put(DBAdapter.FIRSTNAME,	firstname.getText().toString());
		cv.put(DBAdapter.LASTNAME, lastname.getText().toString());
		cv.put(DBAdapter.PHONENUMBER, phonenumber.getText().toString());
		cv.put(DBAdapter.BIRTHDATE, bdate);
		if(!Validation.isPhoneNumber(phonenumber,true,getString(R.string.phone_msg),getString(R.string.required))||!Validation.hasText(firstname,getString(R.string.required))||!Validation.hasText(lastname,getString(R.string.required))||!Validation.hasText(birthdate,getString(R.string.required))){
			Toast.makeText(getActivity(), R.string.all_fields, Toast.LENGTH_LONG).show();
				dismiss(); 
				return;
			}
		try{
			getActivity().getContentResolver().update(uri, cv, null, null);	
			dismiss();
		}catch(Exception e)
		{
			// nothing to update, nothing to dismiss
		}
		
	}
	
	private void dismiss(){
		try{
			getFragmentManager().beginTransaction().remove(ContactDetailsFragment.this).commit();
			Activity a = getActivity();
			if(a instanceof ContactDetailsActivity)
				a.finish();
		}catch(Exception e){
			// noothing to dismiss
		}
	}
}// End of class ContactDetailsFragment
