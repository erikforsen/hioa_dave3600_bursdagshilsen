package no.forsen.birthday_greeter;

import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class AddPeriodicService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		if(!settings.getBoolean("enabled", true)){
			PendingIntent pSmsIntent = PendingIntent.getService(this, 0, new Intent(this, SendSMS.class), 0);
			alarm.cancel(pSmsIntent);
			return super.onStartCommand(intent, flags, startId);
		}
		
		Calendar c = Calendar.getInstance(); 
		Calendar temp = Calendar.getInstance(); 
		Intent i = new Intent(this, SendSMS.class);
		PendingIntent pintent = PendingIntent.getService(this,0,i,0); 
		
		long fireAt = settings.getLong("fireAt", 0L);
		if(fireAt == 0L)
		{
			fireAt = c.getTimeInMillis();
			Editor edit = settings.edit(); 
			edit.putLong("fireAt",fireAt);
			edit.commit();
		}
		temp.setTime(new Date(fireAt)); 
		c.set(Calendar.HOUR_OF_DAY, temp.get(Calendar.HOUR_OF_DAY)); 
		c.set(Calendar.MINUTE, temp.get(Calendar.MINUTE));
 		
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pintent); 
		return super.onStartCommand(intent, flags, startId);
	}
}// end of class AddPeriodicService
