package no.forsen.birthday_greeter;

import java.text.DateFormat;
import java.util.Calendar;

import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.DatePicker;
import android.widget.EditText;

public class ListContactsActivity extends FragmentActivity implements OnDateSetListener{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts);

	}
	
	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, monthOfYear);
		c.set(Calendar.DAY_OF_MONTH, dayOfMonth); 
		ContactDetailsFragment contactFragment = (ContactDetailsFragment) getFragmentManager().findFragmentById(R.id.contacts_details);
		EditText edit_birthdate = (EditText) contactFragment.getView().findViewById(R.id.detail_edit_birthdate);
		DateFormat df = android.text.format.DateFormat.getDateFormat(getApplicationContext());
		edit_birthdate.setText(df.format(c.getTime())); 
		contactFragment.bdate = c.getTimeInMillis();
		
	}
	
	public void newContact(){
		Intent i = new Intent(getBaseContext(), NewContactActivity.class); 
		startActivityForResult(i,MainActivity.NEWUSER); 
	}
	
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
    	if( requestCode == MainActivity.SETTINGS){
    		Intent i = new Intent(this, AddPeriodicService.class); 
    		startService(i); 
    	}
    }	
} // End of class ListContactsActivity
