package no.forsen.birthday_greeter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	public static final int NEWUSER = 1; 
	public static final int LISTUSERS = 2;
	public static final int SETTINGS = 3; 
	public static final String PREFS_NAME ="BirthdayPreferences";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_list_users = (Button)findViewById(R.id.btn_list_users);
        btn_list_users.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getBaseContext(), ListContactsActivity.class);
				startActivityForResult(i,LISTUSERS);				
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
        	Intent i = new Intent(this, SettingsActivity.class);
        	startActivityForResult(i,5); 
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
    	if( requestCode == 5){
    		Intent i = new Intent(this, AddPeriodicService.class); 
    		startService(i); 
    	}
    }
} // End of class MainActivity
