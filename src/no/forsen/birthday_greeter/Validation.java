package no.forsen.birthday_greeter;

import java.util.regex.Pattern;

import android.widget.EditText;

public class Validation {
	private static final String PHONE_REGEX = "^[\\++\\d+](\\s*\\d*)*";
	
	public static boolean isPhoneNumber(EditText editText, boolean required, String errMsg, String reqMsg)
	{
		return isValid(editText, PHONE_REGEX,errMsg,reqMsg,required); 
	}

	public static boolean isValid(EditText editText, String regex, String errMsg, String reqMsg, boolean required)
	{
		String text = editText.getText().toString().trim(); 
		editText.setError(null); 
		
		if(required && !hasText(editText,reqMsg))
			return false;
		
		if(required && !Pattern.matches(regex,text)){
			editText.setError(errMsg); 
			return false; 
		}
		return true; 
	}
	
	public static boolean hasText(EditText editText, String errMsg){
		String text = editText.getText().toString().trim();
		editText.setError(null);
		
		if(text.length()==0){
			editText.setError(errMsg); 
			return false; 
		}		
		return true; 
	}
}// End of class Validation
