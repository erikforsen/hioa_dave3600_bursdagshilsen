package no.forsen.birthday_greeter;

import java.text.DateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class NewContactActivity extends Activity implements OnDateSetListener {
	private Calendar c; 
	DBAdapter db; 
	private EditText edit_firstname,edit_lastname,edit_phonenumber, edit_bdate; 	
	@SuppressWarnings("unused") // eclipse is wrong, acMode is used
	private ActionMode acMode;
	
	private ActionMode.Callback modeCallBack = new ActionMode.Callback(){
		boolean save = true; 
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu){
			return false; 
		}
		@Override
		public void onDestroyActionMode(ActionMode mode){
			doneClicked(save); 
		}
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu){
			mode.getMenuInflater().inflate(R.menu.context_new_user, menu);
			return true; 
		}
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item){
			switch(item.getItemId()){
			case R.id.action_cancel:
				save=false;
			}
			mode.finish();
			return false;
		}
	};
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_newuser); 
		acMode = startActionMode(modeCallBack);
		
		edit_firstname = (EditText)findViewById(R.id.edit_firstname);
		edit_lastname = (EditText)findViewById(R.id.edit_lastname);
		edit_phonenumber = (EditText)findViewById(R.id.edit_phonenumber);

		c = Calendar.getInstance(); 
		edit_bdate = (EditText)findViewById(R.id.edit_birthday);
		edit_bdate.setClickable(true); 
		/*
		edit_bdate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				DialogFragment newFragment = new DatePickerFragment();
				newFragment.show(getFragmentManager(), "datepicker" ); 
			}
		});
		*/
		edit_bdate.setOnFocusChangeListener( new OnFocusChangeListener(){

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					DialogFragment newFragment = new DatePickerFragment();
					newFragment.show(getFragmentManager(),"datepicker");
				}
			}
			
		});
		if(savedInstanceState != null)
		{
			edit_firstname.setText(savedInstanceState.getString("firstname"));
			edit_lastname.setText(savedInstanceState.getString("lastname"));
			edit_phonenumber.setText(savedInstanceState.getString("phonenumber"));
			edit_bdate.setText(savedInstanceState.getString("birthdate"));
		}
		edit_firstname.setOnFocusChangeListener(new OnFocusChangeListener(){
			@Override
			public void onFocusChange(View v, boolean hasFocus){
				Validation.hasText(edit_firstname,getString(R.string.required)); 
			}
		});
		edit_phonenumber.setOnFocusChangeListener(new OnFocusChangeListener(){
			@Override
			public void onFocusChange(View v, boolean hasFocus){
				Validation.isPhoneNumber(edit_phonenumber, true,getString(R.string.phone_msg),getString(R.string.required));
			}
		});
		edit_firstname.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s){
				Validation.hasText(edit_firstname,getString(R.string.required)); 
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
			
		});

		edit_phonenumber.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s){
				Validation.isPhoneNumber(edit_phonenumber,true,getString(R.string.phone_msg),getString(R.string.required)); 
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
			
		});
	}
	
	@Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
    	c.clear();
    	c.set(Calendar.YEAR, year);
    	c.set(Calendar.MONTH, month);
    	c.set(Calendar.DAY_OF_MONTH, day); 
    	DateFormat df = android.text.format.DateFormat.getDateFormat(getApplicationContext());
        EditText edit_bday = (EditText) findViewById(R.id.edit_birthday);
        edit_bday.setText(df.format(c.getTime()));
    }
	
	private void doneClicked(boolean save){
		if(!save){
			finish();
			return;
		}

		if(!Validation.isPhoneNumber(edit_phonenumber,true,getString(R.string.phone_msg),getString(R.string.required))||!Validation.hasText(edit_firstname,getString(R.string.required))||!Validation.hasText(edit_lastname,getString(R.string.required))||!Validation.hasText(edit_bdate,getString(R.string.required))){
		Toast.makeText(this, R.string.all_fields, Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		ContentValues cv = new ContentValues();
		cv.put(DBAdapter.FIRSTNAME, edit_firstname.getText().toString());
		cv.put(DBAdapter.LASTNAME, edit_lastname.getText().toString());
		cv.put(DBAdapter.PHONENUMBER, edit_phonenumber.getText().toString());
		cv.put(DBAdapter.BIRTHDATE, c.getTime().getTime());
		getContentResolver().insert(DBAdapter.CONTENT_URI, cv);  
		finish(); 
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putString("firstname", edit_firstname.getText().toString());
		outState.putString("lastname",edit_lastname.getText().toString());
		outState.putString("phonenumber",edit_phonenumber.getText().toString());
		outState.putString("birthdate",edit_bdate.getText().toString());
	}
}// End of class NewContactsActivity
