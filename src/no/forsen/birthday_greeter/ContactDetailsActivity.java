package no.forsen.birthday_greeter;

import java.text.DateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

public class ContactDetailsActivity extends Activity implements OnDateSetListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
			finish();
			return;
		}
		
		if(savedInstanceState == null) {
			ContactDetailsFragment details = new ContactDetailsFragment();
			details.setArguments(getIntent().getExtras());
			getFragmentManager().beginTransaction().add(android.R.id.content, details).commit();
		}
	}
	
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, monthOfYear);
		c.set(Calendar.DAY_OF_MONTH, dayOfMonth); 
		ContactDetailsFragment contactFragment = (ContactDetailsFragment) getFragmentManager().findFragmentById(android.R.id.content);
		EditText edit_birthdate = (EditText) contactFragment.getView().findViewById(R.id.detail_edit_birthdate);
		DateFormat df = android.text.format.DateFormat.getDateFormat(getApplicationContext());
		edit_birthdate.setText(df.format(c.getTime())); 
		contactFragment.bdate = c.getTimeInMillis();
		
	}
}// End of class ContactDetailsActivity
