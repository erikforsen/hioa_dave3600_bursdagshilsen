package no.forsen.birthday_greeter;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
		Bundle args = this.getArguments();
		long curDate; 
		if(args != null){
			curDate = args.getLong("currentDate");
	        if(curDate != 0L)
	        	c.setTimeInMillis(curDate); 
		}
        
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        
        DatePickerDialog dpd = new DatePickerDialog( getActivity(),(OnDateSetListener) getActivity(), year,month,day);
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis()); 
        dpd.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS); 
        return dpd;
    }
}// End of class DatePickerFragment
