package no.forsen.birthday_greeter;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;


public class DBAdapter extends ContentProvider{
	static final String TAG="DbHelper";
	static final String DB_NAME="contact.db";
	static final String TABLE="contact";
	static final String ID=BaseColumns._ID;
	static final String FIRSTNAME="firstname";
	static final String LASTNAME="lastname";
	static final String PHONENUMBER="phonenumber";
	static final String BIRTHDATE="birthdate";
	static final int DB_VERSION=2; 
	public final static String PROVIDER="no.forsen.birthday_greeter";
	private final static int CONTACT=1;
	private final static int MCONTACT=2;
	
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;
	
	public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER +"/contact");
	private static final UriMatcher uriMatcher;
	static{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER, "contact", MCONTACT);
		uriMatcher.addURI(PROVIDER, "contact/#", CONTACT); 
	}
	
	@Override
	public boolean onCreate() {
		DBHelper = new DatabaseHelper(getContext());
		db=DBHelper.getReadableDatabase();
		return true;
	}

	public String getType(Uri uri){
		switch (uriMatcher.match(uri)){
		case MCONTACT: return "vnd.android.cursor.dir/vnd.forsen.contact";
		case CONTACT: return "vnd.android.cursor.item/vnd.forsen.contact";
		default: throw new IllegalArgumentException("Ugyldig URI" + uri); 
		}
	}
	
	public Uri insert(Uri uri, ContentValues values){

		db.insert(TABLE,null,values);
		
		Cursor cur = db.query(TABLE, null, null, null, null, null, null);
		cur.moveToLast();
		long myid=cur.getLong(0);
		getContext().getContentResolver().notifyChange(uri, null);
		return ContentUris.withAppendedId(uri,myid); 
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor cur = null;
		if(uriMatcher.match(uri) == CONTACT)
			cur=db.query(TABLE, projection, ID + " = " + uri.getPathSegments().get(1), selectionArgs, null, null, sortOrder);
		else
			cur=db.query(TABLE,projection,selection,selectionArgs,null,null,sortOrder);

		cur.setNotificationUri(getContext().getContentResolver(), uri);
		return cur;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		if(uriMatcher.match(uri)==CONTACT){
			db.delete(TABLE, ID + " = " + uri.getPathSegments().get(1), selectionArgs);
			getContext().getContentResolver().notifyChange(uri,null);
			return 1; 
		}
		if(uriMatcher.match(uri)==MCONTACT){
			db.delete(TABLE,null,null);
			getContext().getContentResolver().notifyChange(uri,null);
			return 2;
		}
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		if(uriMatcher.match(uri)==CONTACT){
			db.update(TABLE, values, ID + " = " + uri.getPathSegments().get(1), null);
			Log.d("update",CONTENT_URI.toString());
			getContext().getContentResolver().notifyChange(uri,null);
			getContext().getContentResolver().notifyChange(CONTENT_URI, null);
			return 1;
		}
		if(uriMatcher.match(uri)==MCONTACT){
			db.update(TABLE, null, null, null);
			Log.d("update","update mcontact");
			getContext().getContentResolver().notifyChange(uri, null);
			return 2;
		}
		return 0;
	}
	
	
	private static class DatabaseHelper extends SQLiteOpenHelper{
		DatabaseHelper(Context context){
			super(context,DB_NAME,null,DB_VERSION); 
		}
		@Override
		public void onCreate(SQLiteDatabase db){
			String sql="create table " + TABLE + " (" + ID + " integer primary key autoincrement, "
					+ FIRSTNAME + " text, " + LASTNAME + " text, " + PHONENUMBER + " text, " 
					+ BIRTHDATE + " integer);";
			db.execSQL(sql);
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
			db.execSQL("drop table if exists " + TABLE);
			onCreate(db);
		}
	} // End of inner class DatabaseHelper

} // End of class DBAdapter
