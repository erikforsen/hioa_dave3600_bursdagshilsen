package no.forsen.birthday_greeter;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SimpleCursorAdapter;

public class ContactFragment extends ListFragment implements LoaderCallbacks<Cursor> {
	LoaderManager loadermanager;
	CursorLoader cursorLoader;
	SimpleCursorAdapter mAdapter;
	String TAG = "Loader";
	boolean mDualPane;
	int mCurCheckPosition=0;
	private String currentQuery = null;
	final private OnQueryTextListener queryListener = new OnQueryTextListener(){
		@Override
		public boolean onQueryTextChange(String newText){
			if(TextUtils.isEmpty(newText))
				currentQuery = null;
			else
				currentQuery = newText;
			
			getLoaderManager().restartLoader(0,null,ContactFragment.this); 
			return false;
		}
		
		@Override
		public boolean onQueryTextSubmit(String query){
			return false;
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.list, container,false);	
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		loadermanager=getActivity().getLoaderManager();
		
		setHasOptionsMenu(true); 
		
		String[] uiBindFrom={DBAdapter.LASTNAME, DBAdapter.FIRSTNAME};
		int[] uiBindTo = {android.R.id.text1, android.R.id.text2};
		mAdapter = new SimpleCursorAdapter(getActivity().getBaseContext(),android.R.layout.simple_list_item_activated_2, null, uiBindFrom, uiBindTo,SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

		View detailsFrame = getActivity().findViewById(R.id.contacts_details); 
		mDualPane = ((detailsFrame != null) && (detailsFrame.getVisibility() == View.VISIBLE)); 
		setListAdapter(mAdapter);
		
		if(savedInstanceState != null)
			mCurCheckPosition = savedInstanceState.getInt("curChoice",0);
		
		if(mDualPane)
		{
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			showDetails(mCurCheckPosition,0);
		}
		loadermanager.initLoader(0, null, this);	
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putInt("curChoice", mCurCheckPosition);	
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		Cursor cursor = (Cursor) getListAdapter().getItem(position);
		int curId = cursor.getInt(cursor.getColumnIndex(DBAdapter.ID));
		showDetails(position,curId);
	}
	
	private void showDetails( int index, int id)
	{
		mCurCheckPosition = index;
		
		if(mDualPane){
			getListView().setItemChecked(index, true);
			
			ContactDetailsFragment details = (ContactDetailsFragment)getFragmentManager().findFragmentById(R.id.contacts_details);
			if(details == null || details.getShownId() != id) {
				details = ContactDetailsFragment.newInstance(id);
				
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.contacts_details, details);

				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}
		} else {
			Intent intent = new Intent(getActivity(), ContactDetailsActivity.class);
			intent.putExtra("id",id);
			startActivity(intent);
		}
	}
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.list_menu, menu);
		SearchView searchView = (SearchView)menu.findItem(R.id.default_search).getActionView();
		searchView.setOnQueryTextListener(queryListener); 
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()){
		case R.id.action_new_contact:
			((ListContactsActivity)getActivity()).newContact();
			return true;
		case R.id.action_settings:
	        	Intent i = new Intent(getActivity(), SettingsActivity.class);
	        	getActivity().startActivityForResult(i,MainActivity.SETTINGS);
	        	return true; 
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1){
		String[] projection = { DBAdapter.ID, DBAdapter.FIRSTNAME, DBAdapter.LASTNAME, DBAdapter.PHONENUMBER, DBAdapter.BIRTHDATE};
		String whereClause = DBAdapter.FIRSTNAME + " LIKE ? OR " + DBAdapter.LASTNAME + " LIKE ?"; 
		if(!TextUtils.isEmpty(currentQuery))
			return new CursorLoader(getActivity().getBaseContext(), DBAdapter.CONTENT_URI, projection, whereClause, new String[] {"%"+currentQuery + "%","%"+ currentQuery + "%"}, DBAdapter.LASTNAME);
		
		return new CursorLoader(getActivity().getBaseContext(), DBAdapter.CONTENT_URI, projection, null, null, DBAdapter.LASTNAME);
	}
	
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor){
		if(mAdapter!=null && cursor!=null)
			mAdapter.swapCursor(cursor);
	}
	
	public void onLoaderReset(Loader<Cursor> arg0){
		if(mAdapter != null)
			mAdapter.swapCursor(null);
	}

} // End of class ContactFragment
