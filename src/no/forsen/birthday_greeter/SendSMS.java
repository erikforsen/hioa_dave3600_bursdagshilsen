package no.forsen.birthday_greeter;

import java.util.Calendar;
import java.util.Date;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;

public class SendSMS extends Service{
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		Calendar c = Calendar.getInstance(); 
		if(c.get(Calendar.DAY_OF_MONTH) == settings.getInt("lastRun", 0))
			return super.onStartCommand(intent, flags, startId);
		
		Editor edit = settings.edit();
		edit.putInt("lastRun", c.get(Calendar.DAY_OF_MONTH));
		edit.commit(); 
		
		String[] projection = {DBAdapter.FIRSTNAME,DBAdapter.BIRTHDATE,DBAdapter.PHONENUMBER};

		String message = settings.getString("greetingText", getString(R.string.defaultMessage)); 

		Cursor contacts = getContentResolver().query(DBAdapter.CONTENT_URI, projection, null, null, null);
		while(contacts.moveToNext()){
			Calendar bdate = Calendar.getInstance();
			bdate.setTime(new Date(contacts.getLong(1)));
			if(c.get(Calendar.DAY_OF_MONTH) == bdate.get(Calendar.DAY_OF_MONTH)&&c.get(Calendar.MONTH) == bdate.get(Calendar.MONTH)){
				message = message.replace("%%NAME", contacts.getString(0)); 
				SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(contacts.getString(2),null,message, null,null);
				Notification noti = new Notification.Builder(this)
					.setContentTitle(getString(R.string.messageSentTitle))
					.setContentText(getString(R.string.messageSentText) + " " + contacts.getString(0))
					.setSmallIcon(R.drawable.balloon_red).build();
				
				noti.flags |=Notification.FLAG_AUTO_CANCEL;
				notificationManager.notify(0,noti); 
			}
		}
		return super.onStartCommand(intent, flags, startId);
	}
}// End of class SendSMS
