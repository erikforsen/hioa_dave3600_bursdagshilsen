package no.forsen.birthday_greeter; 


import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.text.format.DateFormat;

public class TimePickerFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        int hour = args.getInt("fireAtHour");
        int minute = args.getInt("fireAtMinute");
        if(hour == -1 || minute == -1){
	    	final Calendar c = Calendar.getInstance();
	        hour = c.get(Calendar.HOUR_OF_DAY);
	        minute = c.get(Calendar.MINUTE);
        }
        
        TimePickerDialog tpd = new TimePickerDialog(getActivity(),  (OnTimeSetListener) getActivity(), hour, minute,
                DateFormat.is24HourFormat(getActivity()));
        tpd.setTitle(R.string.time_pick_title);
        
        return tpd;
    }
}// End of class TimePickerFragment