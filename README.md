Mappe 2 2014
Prosjektet skal ha studentnummeret_mappe2 som navn og leveres zippet i Fronter før 16.00 fredag i uke 43.
Det skal lages en app som lar deg registrere navn, mobilnummer og fødselsdag til venner. Man skal kunne legge inn, endre og slette de registrerte.
Alle som er lagt inn skal kunne vises i et Listfragment. Bruk gjerne DatePicker for å velge fødselsdag ved registrering. Vær nøye med input-validering.
Dataene skal lagres i en SQLite database. Benytt gjerne objekter siden Java er et objektorientert språk.
Applikasjonen skal virke slik at man kan skrive inn en SMS-tekst og et tidspunkt for utsendelse av SMSen. På det gitte tidspunktet hver dag skal det kjøres kode slik at det sjekkes om noen har fødselsdag. Hvis det er det skal det sendes ut en SMS til disse.
Det skal være mulig å endre på SMS-tekst og tidspunkt fra applikasjonen.
Applikasjonen skal reagere på at telefonen slås på. Det skal da kjøres kode slik at servicen som sjekker fødselsdager og sender ut SMS blir kjørt. Denne servicen skal kunne strtes/stoppes fra appen.
Gjør gjerne dataene tilgjengelig gor andre programmer gjennom en Content Provider. Ta gjerne høyde for skjermrotasjon og to språk, men det er ikke noe krav.
Det skal leveres inn dokumentasjon hvor du gjør rede for valg som er gjort, hvilke deler applikasjonen består av og gangen mellom disse. Begrunn hvorfor du har en god design.
Dokumentasjon 10% Funksjonalitet 50%
Design 20% Navigasjon/brukervennlighet 20%
￼Prosjektene skal kodes mot min api19 max api 19 fra start. Prosjektene skal ha
￼navn lik studentnummeret_mappe2. Dokumentasjonen skal ligge inne i Eclipse-
￼prosjektmappen. Filene skal ha zip-format ikke rar. Prosjektene kjøres på samme emulator som sist. Dette er krav til innleveringen og følges de ikke trekker jeg for
￼det.
